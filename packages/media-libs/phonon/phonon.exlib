# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2010 Ingmar Vanhassel
# Copyright 2013-2014, 2016, 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'phonon-4.2-scm.kdebuild-1' from Genkdesvn, which is:
#     Copyright 2007-2008 by the individual contributors of the genkdesvn project
#     Based in part upon the respective ebuild in Gentoo which is:
#     Copyright 1999-2008 Gentoo Foundation

require kde.org kde [ translations=qt ]

SUMMARY="KDE multimedia API"
HOMEPAGE="https://phonon.kde.org/"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
SLOT="0"
MYOPTIONS="doc
    designer   [[ description = [ Install Qt Designer plugins ] ]]
    gstreamer  [[ description = [ Enable the Phonon Gstreamer backend ] ]]
    pulseaudio [[ description = [ Enable playback through Pulseaudio ] ]]
    vlc        [[ description = [ Use the Phonon VLC backend, which is the recommended backend ] ]]

    ( gstreamer vlc ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=5.60.0]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        x11-libs/qtbase:5
        designer? ( x11-libs/qttools:5 )
        pulseaudio? (
            dev-libs/glib:2
            media-sound/pulseaudio[>=0.9.21]
        )
    post:
        gstreamer? ( media-libs/phonon-gstreamer )
        vlc? ( media-libs/phonon-vlc )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPHONON_BUILD_SETTINGS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'designer PHONON_BUILD_DESIGNER_PLUGIN'
    'doc PHONON_BUILD_DOC'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'designer Qt5Designer'
    'pulseaudio GLIB2'
    'pulseaudio PulseAudio'
)
CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DATAROOTDIR:PATH=/usr/share
)

