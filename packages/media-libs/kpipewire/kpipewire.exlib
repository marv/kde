# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ group_or_user=plasma ] kde

SUMMARY="Components relating to Flatpak 'pipewire' use in Plasma"
DESCRIPTION="
Offers a set of convenient classes to use PipeWire (https://pipewire.org/) in
Qt projects.
It is developed in C++ and it's main use target is QML components."

LICENCES="
    BSD-3 [[ note = [ cmake/Findlibdrm.cmake ] ]]
    CC0   [[ note = [ dot files ] ]]
    || ( LGPL-2.1 LGPL-3.0 )
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.94.0"

DEPENDENCIES="
    build:
        kde/plasma-wayland-protocols
        virtual/pkg-config
    build+run:
        dev-libs/libepoxy[>=1.3]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        media/ffmpeg
        media/pipewire[>=0.3]
        x11-dri/libdrm[>=2.4.62]
        x11-dri/mesa
        sys-libs/wayland
        x11-libs/qtbase:5
        x11-libs/qtdeclarative:5
        x11-libs/qtwayland:5
"

