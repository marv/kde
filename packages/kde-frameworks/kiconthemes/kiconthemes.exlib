# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Icon theme GUI utilities"
DESCRIPTION="
This library contains classes to improve the handling of icons
in applications using the KDE Frameworks. Provided are:
- KIconDialog: Dialog class to let the user select an icon
    from the list of installed icons.
- KIconButton: Custom button class that displays an icon.
    When clicking it, the user can change it using the icon dialog.
- KIconEffect: Applies various colorization effects to icons,
    which can be used to create selected/disabled icon images."

LICENCES="LGPL-2.1"
MYOPTIONS="
    designer [[ description = [ Install Qt designer plugins ] ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
    test:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

kiconthemes_src_test() {
    xdummy_start

    default

    xdummy_stop
}

