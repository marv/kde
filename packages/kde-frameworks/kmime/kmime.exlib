# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="A library for handling mail messages and newsgroup articles"
DESCRIPTION="
KMime deals solely with the in-memory representation of messages, topics such
as transport or storage of messages are handled by other libraries, for
example by the mailtransport library the KIMAP library.
Similary, this library does not deal with displaying messages or advanced
composing, for those there messageviewer and the messagecomposer components in
the KDEPIM module.
KMime's main function is to parse, modify and assemble messages in-memory."

LICENCES="LGPL-2.1"
SLOT="5"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.95.0
else
    KF5_MIN_VER=5.91.0
fi
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

