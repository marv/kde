# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde

SUMMARY="Desktop style for QtQuickControls 2"
DESCRIPTION="
This is a style for QtQuickControls 2 that uses QWidget's QStyle for painting,
making it possible to achieve an higher deree of consistency between
QWidget-based and QML-based apps."

LICENCES="GPL-2 LGPL-3"
SLOT="0"
MYOPTIONS="X"

QT_MIN_VER=5.7.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        (
            kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
            kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        ) [[ note = [ might be optional, integration of Kirigami with Plasma desktop ] ]]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        X? (
            x11-libs/libX11
            x11-libs/qtx11extras:5
        )
    run:
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X X11' )

