# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Allows LDAP accessing with a convenient Qt style C++ API"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
SLOT="5"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.95.0
else
    KF5_MIN_VER=5.91.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        net-directory/openldap
        net-libs/cyrus-sasl
        sys-auth/qtkeychain[providers:qt5]
        x11-libs/qtbase:5
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

