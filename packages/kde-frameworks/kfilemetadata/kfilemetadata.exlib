# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if [[ $(ever major) == 6 ]] ; then
    major_version=6
else
    major_version=5
fi

require kde-frameworks kde [ kde_major_version=${major_version} translations='ki18n' ]

export_exlib_phases src_prepare src_test

SUMMARY="A library for extracting file metadata"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2 LGPL-2.1 LGPL-3"

MYOPTIONS="
    appimage [[ description = [ Support for extracting metadata from AppImage files ] ]]
    epub     [[ description = [ Support for extracting metadata from .epub ebook files ] ]]
    exif     [[ description = [ Support for extracting metadata from EXIF headers ] ]]
    ffmpeg   [[ description = [ Support for extracting audio and video metadata ] ]]
    mobipocket [[ description = [ Support for extracting metadata from Mobipocket e-books ] ]]
    pdf      [[ description = [ Support for extracting metadata from pdf files ] ]]
    taglib   [[ description = [ Support for extracting metadata from music files ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        sys-apps/attr
        appimage? (
            dev-libs/libappimage[>=0.1.10]
            kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        )
        epub? ( app-text/ebook-tools )
        exif? ( graphics/exiv2[>=0.21] )
        ffmpeg? ( media/ffmpeg[>=3.1] )
        mobipocket? ( kde/mobipocket:4[>=2.0] )
        pdf? ( app-text/poppler[>=0.12.1][qt5] )
        taglib? ( media-libs/taglib[>=1.11.1] )
    test:
        dev-lang/python:*
    suggestion:
        app-doc/catdoc [[ description = [ Extract text from office 98 files ] ]]
"

if [[ ${major_version} == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qt5compat:6
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'appimage KF5Config'
    'appimage libappimage'
    EPub
    'exif LibExiv2'
    FFmpeg
    'mobipocket QMobipocket'
    'pdf Poppler'
    Taglib
)

kfilemetadata_src_prepare() {
    kde_src_prepare

    # Test fails under sydbox
    edo sed -e '/^# UserMetaData/,+8d' \
            -i autotests/CMakeLists.txt
}

kfilemetadata_src_test() {
    # propertyinfotest hard codes this
    LC_ALL=en_US.UTF-8 emake test
}

