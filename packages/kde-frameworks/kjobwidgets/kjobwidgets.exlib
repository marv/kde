# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if [[ $(ever major) == 6 ]] ; then
    major_version=6
else
    major_version=5
fi

require kde-frameworks kde [ kde_major_version=${major_version} translations='qt' ]

export_exlib_phases src_install

SUMMARY="Widgets for showing progress of asynchronous jobs"

LICENCES="LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        X? (
            x11-libs/libX11
        )
"

if [[ ${major_version} == 5 ]] ; then
    DEPENDENCIES+="
        build+run:
            X? ( x11-libs/qtx11extras:5[>=${QT_MIN_VER}] )
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X X11' )

kjobwidgets_src_install() {
    cmake_src_install

    if [[ ${major_version} == 6 ]] ; then
        # interfaces/kf5_org.kde.JobView{,Server,V2}.xml
        edo rm -r "${IMAGE}"/usr/share/dbus-1
    fi
}

