# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if [[ $(ever major) == 6 ]] ; then
    major_version=6
else
    major_version=5
fi

require kde-frameworks kde [ kde_major_version=${major_version} translations='qt' ]
require test-dbus-daemon

export_exlib_phases src_install

SUMMARY="KDE Desktop notifications framework"
DESCRIPTION="
KNotification is used to notify the user of an event. It covers feedback and
persistent events.
"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    LGPL-2.1
"
MYOPTIONS="
    canberra [[ description = [ Use libcanberra to play event sounds ] ]]
    phonon   [[ description = [ Use phonon to play event sounds ] ]]
    tts      [[ description = [ Support for text to speech ] ]]
    X   [[ presumed = true ]]

    ( canberra phonon ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/dbusmenu-qt[>=0.9.3_p259][qt5(+)]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        canberra? ( media-libs/libcanberra )
        X? (
            x11-libs/libX11 [[ note = [ Could make this optional if necessary ] ]]
            x11-libs/libXtst [[ note = [ Could make this optional if necessary ] ]]
        )
"

if [[ ${major_version} == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            phonon? ( media-libs/phonon[>=4.6.60][providers:qt6] )
    "
elif [[ ${major_version} == 5 ]] ; then
    DEPENDENCIES+="
        build+run:
            phonon? ( media-libs/phonon[>=4.6.60][qt5(+)] )
            tts? ( x11-libs/qtspeech:5 )
            X? (
                x11-libs/qtx11extras:5[>=${QT_MIN_VER}] [[ note = [ Could make this optional if necessary ] ]]
            )
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    Canberra
    'phonon Phonon4Qt5'
    'tts Qt5TextToSpeech'
    'X X11'
)

knotifications_src_install() {
    cmake_src_install

    if [[ ${major_version} == 6 ]] ; then
        # /usr/share/dbus-1/interfaces/kf5_org.kde.StatusNotifier{Item,Watcher}.xml
        edo rm -r "${IMAGE}"/usr/share/dbus-1
    fi
}

