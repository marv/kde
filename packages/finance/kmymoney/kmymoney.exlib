# Copyright 2009 Ingmar Vanhassel
# Copyright 2012,2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] kde [ translations='ki18n' ]
require toolchain-funcs
require python [ blacklist=2 multibuild=false with_opt=true option_name=weboob ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Personal finance manager for KDE"
DESCRIPTION="
KMyMoney is the personal finance manager for KDE. It operates in a manner similar to MS Money and
Quicken and supports different account types, categorisation of expenses, QIF import/export,
multiple currencies, and online banking.
"
HOMEPAGE="https://www.kmymoney.org/"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news.php#itemKMyMoney$(ever delete_all)released"
LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2"
SLOT="0"
MYOPTIONS="
    addressbook [[ description = [ Access information from addressbooks stored with akonadi ] ]]
    calendar    [[ description = [ Allows you to export scheduled transactions to an iCal file ] ]]
    kbanking    [[ description = [ Enables online banking over HBCI ] ]]
    ofx
    sqlcipher   [[ description = [ Transparently encrypts data saved to SQLite databases ] ]]
    webengine   [[ description = [ Prefer QtWebEngine over KF5WebKit ] ]]
    weboob      [[ description = [ Use online banking without a browser via Weboob ] ]]
"

KF5_MIN_VER=5.42
QT_MIN_VER=5.6.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=1.7.0]
        dev-libs/boost[>=1.33.1]
        dev-libs/glib:2
        dev-libs/gmp:=
        dev-libs/libxml2:2.0
        finance/libalkimia[>=7.0]
        kde/kdiagram[>=2.6.0]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}] [[ note = [ could be optional ] ]]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info
        addressbook? (
            kde-frameworks/kcontacts:5
            kde-frameworks/kidentitymanagement:5
            server-pim/akonadi:5[>=16.03.80]
        )
        calendar? ( office-libs/libical:=[>=0.33] )
        kbanking? (
            dev-libs/aqbanking[>=6.0.1]
            sys-libs/gwenhywfar[>=5.1.2][qt5]
            x11-libs/qtdeclarative:5
        )
        ofx? (
            finance/libofx[>=0.9.11]  [[ note = [ .11 for clientuid, #366326 ] ]]
        )
        sqlcipher? ( app-crypt/sqlcipher )
        webengine? ( x11-libs/qtwebengine:5[>=5.8] )
       !webengine? (
            kde-frameworks/kdewebkit:5
            x11-libs/qtwebkit:5
        )
        weboob? ( www-client/weboob )
    recommendation:
        kde/kcalc:4 [[ description = [ Required for the calculator button in the KMyMoney GUI ] ]]
"

# Disables API docs, Handbooks and man pages are still installed without doxygen
CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
    # Tarballs already have the translations
    -DFETCH_TRANSLATIONS:BOOL=FALSE
    -DPython3_EXECUTABLE:PATH=${PYTHON}
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    'calendar LIBICAL'
    'kbanking KBANKING'
    'ofx OFXIMPORTER'
    'SQLCIPHER'
    'WEBENGINE'
    'weboob WOOB'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'addressbook KF5Akonadi'
    'addressbook KF5Contacts'
    'addressbook KF5IdentityManagement'
    'weboob Python3'
)

kmymoney_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kmymoney_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

