# Copyright 2011 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2013-2015 Heiko Becker <heirecka@eherbo.org>
# Distributed under the terms of the GNU General Public License v2

require telepathy-kde [ telepathy_qt_ver_min=0.9.8 ] gtk-icon-cache

export_exlib_phases src_compile src_install

SUMMARY="Re-used KDE Telepathy Parts"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    accounts [[ description = [ Build plugin for the Accounts and SSO (Single Sign-On) framework ] ]]
    doc
    otr [[ description = [ Support OTR encryption ] ]]
"

KF5_MIN_VER="5.11.0"

DEPENDENCIES+="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpeople:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        net-im/telepathy-logger-qt[>=0.9.80]
        x11-libs/qtbase:5[sql]
        x11-libs/qtdeclarative:5
        accounts? (
            kde/kaccounts-integration:4
            net-libs/accounts-qt[>=1.10]
            net-libs/signon[>=8.55][-qt4]
        )
        otr? (
            dev-libs/libgcrypt
            net-libs/libotr[>=4.0.0]
        )
    run:
        accounts? ( net-im/telepathy-accounts-signon )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'accounts KAccounts'
    'accounts AccountsQt5'
    'accounts SignOnQt5'
    'doc Doxygen'
    'otr LibOTR'
)

ktp-common-internals_src_compile() {
    default

    option doc && emake apidox
}

ktp-common-internals_src_install() {
    default

    option doc && dodoc -r KTp/docs/html/*
}

