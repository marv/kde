# Copyright 2017-2019, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based upon 'qupzilla.exlib', which is:
# Copyright 2012-2014 Bernd Steinhauser <berniyh@exherbo.org>

require kde-apps kde [ translations=ki18n ]
require python [ blacklist=2 multibuild=false ]
require freedesktop-desktop gtk-icon-cache flag-o-matic
require xdummy [ phase=test ]

export_exlib_phases pkg_setup src_configure src_install src_test \
    pkg_postinst pkg_postrm

SUMMARY="Cross-platfrom Qt Web Browser (previously known as QupZilla)"
HOMEPAGE="https://commits.kde.org/falkon"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    bash-completion
    gnome [[ description = [ Support for storing passwords in Gnome keyring ] ]]
    kde [[ description = [ Support for storing passwords in kwallet ] ]]
    python [[ description = [ Support for python plugins ] ]]
    zsh-completion
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER="5.78.0"
QT_MIN_VER=5.15.0

# There is NO_X11, so in principle, it would be possible to build falkon
# without support for X11, but as qt5 links to libX11 currently, there is no
# point in doing so.
DEPENDENCIES="
    build:
        sys-devel/gettext
        x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = [ lrelease ] ]]
        gnome? ( virtual/pkg-config )
    build+run:
        app-spell/hunspell:=
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}] [[ note = [ quickwidgets ] ]]
        x11-libs/qtwebchannel:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}] [[ note = [ webenginewidgets ] ]]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util
        gnome? ( gnome-desktop/gnome-keyring:1 )
        kde? (
            kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
            kde-frameworks/kio:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
            kde-frameworks/purpose:5[>=${KF5_MIN_VER}]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        python? (
            dev-python/PySide2[python_abis:*(-)?][webengine]
            dev-python/shiboken2[python_abis:*(-)?]
        )
       !net-www/qupzilla [[
            description = [ Still installs the same icons as Qupzilla ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde-frameworks/breeze-icons:5 [[ note = [ Used in preferences ] ]]
"

DEFAULT_SRC_TEST_PARAMS+=(
    ARGS+="-E '(pyfalkon-basictest|falkon-qml-qmltabsapitest)'"
)

falkon_pkg_setup() {
    # Fixes an build error with LibreSSL and ld.bfd, cf.
    # https://bugreports.qt.io/browse/QTBUG-63291
    append-ldflags -Wl,--no-fatal-warnings
}

falkon_src_configure() {
    local cmake_args=(
        -DPYTHON_EXECUTABLE=${PYTHON}
        -DPython3_LIBRARY=/usr/$(exhost --target)/lib/libpython$(python_get_abi).so
        -DPython3_INCLUDE_DIR=$(python_get_incdir)

        $(cmake_disable_find kde KF5CoreAddons)
        $(cmake_disable_find kde KF5Crash)
        $(cmake_disable_find kde KF5KIO)
        $(cmake_disable_find kde KF5Purpose)
        $(cmake_disable_find kde KF5Wallet)
        $(cmake_disable_find python PySide2)
        $(cmake_disable_find python Python3)
        $(cmake_disable_find python Shiboken2)

        $(cmake_build gnome KEYRING)
    )

    ecmake "${cmake_args[@]}"
}

falkon_src_install() {
    default

    if ! option bash-completion; then
        edo rm "${IMAGE}"/usr/share/bash-completion/completions/falkon
        edo rmdir "${IMAGE}"/usr/share/bash-completion/{completions,}
    fi

    if option zsh-completion; then
        dodir /usr/share/zsh/site-functions/
        insinto /usr/share/zsh/site-functions/
        doins "${CMAKE_SOURCE}"/linux/completion/_falkon
    fi
}

falkon_src_test() {
    xdummy_start

    default

    xdummy_stop
}

falkon_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

falkon_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

