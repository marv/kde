# Copyright 2009 Ingmar Vanhassel
# Copyright 2011, 2014 Bernd Steinhauser
# Distributed under the terms of the GNU General Public License v2

require sourceforge
require kde.org kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="A user friendly TeX/LaTeX editor for the KDE desktop environment"
DESCRIPTION="
Main Features:
- Compile, convert and view your document with one click.
- Auto-completion of (La)TeX commands
- Templates and wizards make starting a new document very little work.
- Easy insertion of many standard tags and symbols and the option to define (an arbitrary number of) user defined tags.
- Inverse and forward search: click in the DVI viewer and jump to the corresponding LaTeX line in the editor, or jump
  from the editor to the corresponding page in the viewer.  Finding chapter or sections is very easy, Kile constructs
  a list of all the chapter etc. in your document. You can use the list to jump to the corresponding section.
- Collect documents that belong together into a project.
- Easy insertion of citations and references when using projects.
- Flexible and smart build system to compile your LaTeX documents.
- QuickPreview, preview a selected part of your document.
- Easy access to various help sources.
- Advanced editing commands.
"

ever is_scm || \
DOWNLOADS="https://sourceforge.net/projects/${PN}/files/unstable/${PN}-3.0b3/${PNV}.tar.bz2"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 FDL-1.2
"
SLOT="0"
MYOPTIONS="pdf [[ description = [ Support for PDF file operations ] ]]"

KF5_MIN_VER=5.31.0

DEPENDENCIES="
    build:
        dev-lang/perl:*
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext [[ note = [ Used to build the documentation ] ]]
        virtual/pkg-config
    build+run:
        kde/okular:4[>=16.03.0]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.6.0]
        x11-libs/qtscript:5[>=5.6.0]
        pdf? ( app-text/poppler[qt5] )
    run:
        kde/kate:4[>=14.12.0]
    recommendation:
        app-text/texlive-core [[ description = [ Needed to produce dvi/pdf from (La)TeX sources ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'pdf Poppler'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( kile-remote-control.txt )

kile_src_prepare() {
    kde_src_prepare

    edo sed -e "/DESTINATION.*\}/s:/doc/kile:/doc/kile-${PV}:" \
            -i CMakeLists.txt
}

kile_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kile_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

