# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="A command line interface to KDE calendars"
DESCRIPTION="
It lets you view, insert, remove, or modify calendar events by way of the
command line or from a scripting language. Additionally, konsolekalendar can
create a new KDE calendar, export a KDE calendar to a variety of other
formats, and import another KDE calendar."

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.95.0
else
    KF5_MIN_VER=5.91.0
fi
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/calendarsupport[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
"

