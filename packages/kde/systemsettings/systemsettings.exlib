# Copyright 2014-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="KDE's system configuration and administration center"

LICENCES="GPL-2 LGPL-2.1 LGPL-3"
SLOT="4"
MYOPTIONS=""

if ever at_least 5.25.2 ; then
    KF5_MIN_VER="5.94.0"
    QT_MIN_VER="5.15.2"
else
    KF5_MIN_VER="5.87.0"
    QT_MIN_VER="5.15.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libkworkspace[>=${PV}]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kactivities-stats:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=2.1]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    suggestion:
        kde/kde-gtk-config[>=2.9.90]     [[ description = [ Allows to configure appearance of GTK applications ] ]]
        sys-auth/polkit-kde-agent[>=5.1.95] [[ description = [ KDE authentication GUI for PolicyKit ] ]]
"

if ever at_least 5.24.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
    "
fi

