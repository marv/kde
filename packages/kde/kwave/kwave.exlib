# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Kwave is a sound editor built on KDE Frameworks"
DESCRIPTION="
With Kwave you can record, play back, import and edit many sorts of audio
files including multi-channel files.
Kwave includes some plugins to transform audio files in several ways and
presents a graphical view with a complete zoom- and scroll capability.
"

LICENCES="
    BSD-2   [[ note = [ Parts of plugins/codec_ogg/OpusEncoder.cpp ] ]]
    BSD-3   [[ note = [ cmake scripts ] ]]
    FDL-1.2 [[ note = [ docs and screenshots ] ]]
    GPL-2
    LGPL-2  [[ note = [ some icons ] ]]
    || ( CCPL-Attribution-ShareAlike-3.0 GPL-3 ) [[ note = [ Oxygen icons ] ]]
    CCPL-Attribution-ShareAlike-3.0 [[ note = [ audio samples ] ]]
    CC0     [[ note = [ appstream metadata ] ]]
"
SLOT="0"
MYOPTIONS="
    alsa         [[ description = [ Enable playback/recoding via ALSA ] ]]
    doc
    flac
    mp3
    ogg
    pulseaudio   [[ description = [ Enable playback/recoding via PulseAudio ] ]]
    qtmultimedia [[ description = [ Enable playback via QtMultimedia ] ]]
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.90.0
    QT_MIN_VER=5.15.0
else
    KF5_MIN_VER=5.33.0
    QT_MIN_VER=5.14.0
fi

DEPENDENCIES="
    build:
        dev-lang/perl:*
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext
        virtual/pkg-config
        doc? (
            app-text/docbook-xml-dtd:4.5
            gnome-desktop/librsvg:2 [[ note = [ rsvg-convert, ImageMagick would work, too ] ]]
        )
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        media-libs/audiofile[>=0.3.0]
        media-libs/libsamplerate[>=0.1.3]
        sci-libs/fftw[>=3.0]
        x11-libs/qtbase:5[>=5.4.0][gui]
        alsa? ( sys-sound/alsa-lib )
        flac? ( media-libs/flac[>=1.2.0] )
        mp3? (
            media-libs/id3lib[>=3.8.1]
            media-libs/libmad
        )
        ogg? (
            media-libs/libogg[>=1.0.0]
            media-libs/libvorbis[>=1.0.0]
            media-libs/opus[>=1.0.0]
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
        qtmultimedia? ( x11-libs/qtmultimedia:5[>=5.4.0] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DWITH_OSS:BOOL=TRUE )

CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
    ALSA
    DOC
    FLAC
    MP3
    'ogg OGG_VORBIS'
    'ogg OGG_OPUS'
    PULSEAUDIO
    'qtmultimedia QT_AUDIO'
)

kwave_pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    freedesktop_pkg_postinst
}

kwave_pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    freedesktop_pkg_postrm
}

