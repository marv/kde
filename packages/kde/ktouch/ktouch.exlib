# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="KDE EDU: Touch Typing Tutor"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.90.0
else
    KF5_MIN_VER=5.83.0
fi
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libxkbfile
        x11-libs/qtbase:5[sql][>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=5.11.0] [[ note = [ QtQuickCompiler ] ]]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
    run:
        kde/kqtquickcharts:${SLOT}[>=16.11.80] [[ note = [ org.kde.charts.*qml ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCOMPILE_QML:BOOL=TRUE
)

