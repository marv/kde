# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop freedesktop-mime gtk-icon-cache

SUMMARY="A collection manager for KDE"
DESCRIPTION="
Tellico makes it easy to track your books, videos, music, even your wine and anything else. A simple
and intuitive interface shows cover images, groupings, and any detail you want. Grab information
from many popular Internet sites, including IMDB.com, Amazon.com, and most libraries.
"
HOMEPAGE="http://tellico-project.org/"
DOWNLOADS="${HOMEPAGE}files/${PNV}.tar.xz"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    bibtex [[ description = [ Support for importing data via BibTeX ] ]]
    cddb
    cdtext  [[ description = [ Support for reading cdtext form audio CDs ] ]]
    charts  [[ description = [ Adds charts to the report dialog ] ]]
    khtml   [[ description = [ Build against KHTML instead of QtWebEngine ] ]]
    pdf     [[ description = [ Support for reading PDFs and PDF/XMP metadata ] ]]
    scanner [[ description = [ Support adding scanned images to a collection ] ]]
    ( linguas:
        ast bg bs ca ca@valencia cs da de el en_GB eo es et eu fi fr ga gl hu
        ia it ja kk lt mr ms nb nds nl nn pl pt pt_BR ro ru sk sl sv tr ug uk
        zh_CN zh_TW
    )
"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        kde-frameworks/karchive:5
        kde-frameworks/kcodecs:5
        kde-frameworks/kcompletion:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kcrash:5
        kde-frameworks/kfilemetadata:5 [[ note = [ possibly optional ] ]]
        kde-frameworks/kguiaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/kitemmodels:5
        kde-frameworks/kjobwidgets:5
        kde-frameworks/knewstuff:5 [[ note = [ possibly optional ] ]]
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwallet:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/kxmlgui:5
        kde-frameworks/solid:5
        kde-frameworks/sonnet:5
        media-libs/taglib
        x11-libs/qtbase:5[>=5.4.0]
        charts? ( x11-libs/qtcharts:5[>=5.4.0] )
        cddb? ( kde/libkcddb:5 )
        cdtext? ( dev-libs/libcdio )
        khtml? ( kde-frameworks/khtml:5 )
       !khtml? ( x11-libs/qtwebengine:5[>=5.4.0] )
        pdf? (
            app-text/poppler[qt5]
            media-libs/exempi:2.0[>=2.0]
        )
        scanner? ( kde/libksane:5 )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_FETCHER_TESTS:BOOL=FALSE
    -DENABLE_CDTEXT:BOOL=TRUE
    -DENABLE_WEBCAM:BOOL=FALSE
    # Contained in dev-perl/Text-BibTeX, but that doesn't install a btparse.h
    # header, thus it can't be found. So we use the bundled copy for now.
    -DCMAKE_DISABLE_FIND_PACKAGE_Btparse:BOOL=TRUE
    # External support for reading CSV files
    -DCMAKE_DISABLE_FIND_PACKAGE_Csv:BOOL=TRUE
    # Searching z39.50 databases
    -DCMAKE_DISABLE_FIND_PACKAGE_Yaz:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'bibtex BTPARSE'
)
CMAKE_SRC_CONFIGURE_OPTION_USES=(
    KHTML
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'cddb KF5Cddb'
    'cdtext CDIO'
    'charts Qt5Charts'
    'pdf Poppler'
    'pdf Exempi'
    'scanner KF5Sane'
)

# Skip tests which want to create a kioslave
DEFAULT_SRC_TEST_PARAMS+=(
    ARGS+="-E '(filelisting|goodreads|htmlexporter|imagejob|newstufftest|tellicoread)'"
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm () {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

