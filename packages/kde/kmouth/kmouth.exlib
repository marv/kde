# Copyright 2012, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeaccessibility.exlib', which is:
#     Copyright 2010 Yury G. Kudryashov and 2010-2011 Bo Ørsted Andresen

require kde-apps kde gtk-icon-cache

SUMMARY="Speech Synthesizer Frontend"
DESCRIPTION="A program which enables persons that cannot speak to let their computer speak, e.g.
mutal people or people who have lost their voice. It has a text input field and speaks the
sentences that you enter. It also has support for user defined phrasebooks."

HOMEPAGE="https://www.kde.org/applications/utilities/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

if ever at_least 22.07.80 ; then
    KF5_MIN_VER="5.90.0"
else
    KF5_MIN_VER="5.85.0"
fi
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtspeech:5[>=${QT_MIN_VER}]
"

