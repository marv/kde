# Copyright 2011 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde freedesktop-desktop

SUMMARY="Thumbnailers for various graphics file formats"

LICENCES="GPL-2 LGPL-2"
MYOPTIONS="
    raw [[ description = [ Generate thumbnails from RAW images ] ]]
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER="5.90.0"
else
    KF5_MIN_VER="5.83.0"
fi
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build+run:
        kde/mobipocket:4
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        raw? (
            kde-frameworks/libkdcraw:5
            kde-frameworks/libkexiv2:5
        )
"

# Blender thumbnailer, needs karchive, but dep and code are small
CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_blend:BOOL=TRUE )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'raw KF5KExiv2'
    'raw KF5KDcraw'
)

