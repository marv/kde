# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma-mobile kde
require python [ blacklist=2 multibuild=false ]
require gtk-icon-cache

SUMMARY="A client for YouTube Music"
DESCRIPTION="AudioTube can search YouTube Music, list albums and artists, play
automatically generated playlists, albums and allows to put your own playlist
together."

LICENCES="
    BSD-2 [[ note = [ cmake scripts ] ]]
    CC0 [[ note = [ desktop files, etc ] ]]
    || ( GPL-2 GPL-3 )
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.81.0"
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build+run:
        dev-libs/pybind11[python_abis:*(-)?]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        dev-python/ytmusicapi[>=0.19.1][python_abis:*(-)?]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:soup]
        net-misc/yt-dlp
        x11-libs/qtimageformats:5[>=${QT_MIN_VER}]
"
# NOTE: Probably should at least recommend some other gst-plugins-* options
# but I  don't have a list of all the formats YouTube/YouTube Music uses.

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DPython3_EXECUTABLE:PATH=${PYTHON}
)

