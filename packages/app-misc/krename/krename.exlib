# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] kde [ translations='ki18n' ]

SUMMARY="Powerful batch renamer for KDE"
DESCRIPTION="
KRename is a powerful batch renamer for KDE. It allows you to easily rename hundreds or even more
files in one go. The filenames can be created using parts of the original filename, by numbering
the files, or from file metadata like creation date or EXIF information of an image.
"
HOMEPAGE="https://userbase.kde.org/KRename"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2"
SLOT="0"
MYOPTIONS="
    pdf [[ description = [ Build plugin to use information from PDFs via libpodofo to rename a file ] ]]
    taglib     [[ description = [ Support for extracting metadata from music files ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        graphics/exiv2[>=0.13]
        kde-frameworks/kcompletion:5
        kde-frameworks/kconfig:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kcrash:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/kitemviews:5
        kde-frameworks/kjobwidgets:5
        kde-frameworks/kjs:5
        kde-frameworks/kservice:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        media-libs/freetype:2
        x11-libs/qtbase:5
        pdf? ( app-text/podofo )
        taglib? ( media-libs/taglib[>=1.5] )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'pdf PoDoFo'
    Taglib
)

