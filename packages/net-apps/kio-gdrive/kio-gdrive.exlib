# Copyright 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="A KIO slave that allows KIO-aware applications to access Google Drive files"

HOMEPAGE=" https://community.kde.org/KIO_GDrive"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    share [[ description = [ Enables the Share menu plugin ] ]]
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER=5.90.0
else
    KF5_MIN_VER=5.71.0
fi
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build:
        dev-util/intltool
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        app-pim/libkgapi[>=19.08.41] [[ note = [ aka 5.11.41 ] ]]
        kde/kaccounts-integration:4[>=20.03.80]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        share? ( kde-frameworks/kio:5[>=${KF5_MIN_VER}] )
    recommendation:
        kde/dolphin:4 [[ description = [
            Needed to setup a Google Drive Account and for integration
        ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'share KF5Purpose' )

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

