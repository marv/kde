# Copyright 2013 Friedrich Kröner <friedrich@mailstation.de>
# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-kde"

require kde-apps kde [ translations='ki18n' ]
require test-dbus-daemon freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare src_test pkg_postinst pkg_postrm

SUMMARY="A daemon and KCM to connect your Android device with your desktop"
DESCRIPTION="
Integrate Android with the KDE Desktop.
Current feature list:
- Clipboard share: copy from or to your desktop,
- Notifications sync (4.3+): Read your Android notifications from KDE.
- Multimedia remote control: Use your phone as a remote control.
- WiFi connection: no usb wire or bluetooth needed.
- RSA Encryption: your information is safe.

You can get the matching Android app from:
https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp
"
HOMEPAGE="https://community.kde.org/KDEConnect"

LICENCES="GPL-2 || ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="
    doc
    bluetooth [[ description = [ Use kdeconnect with bluetooth connected devices ] ]]
    pulseaudio
    zsh-completion
"

if ever at_least 22.07.80 ; then
    KF5_MIN_VER="5.95.0"
else
    MYOPTIONS+="
        wayland   [[ description = [ Integrate with KWayland for fake input support ] ]]
    "

    KF5_MIN_VER="5.89.0"
fi
QT_MIN_VER="5.10.0"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        app-crypt/qca:2[>=2.1.0][providers:qt5] [[ note = [ library and qca-ossl plugin ] ]]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpeople:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5(+)]
        sys-apps/dbus
        sys-libs/wayland[>=1.15]
        x11-libs/libfakekey
        x11-libs/libX11
        x11-libs/libXtst
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtwayland:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        bluetooth? ( x11-libs/qtconnectivity:5[>=${QT_MIN_VER}] )
        pulseaudio? ( kde/pulseaudio-qt:= )
    run:
        kde-frameworks/kirigami:2
        kde-frameworks/qqc2-desktop-style[>=${KF5_MIN_VER}]
    recommendation:
        kde/kpeoplevcard [[
            description = [ Read vcards from the file system ]
        ]]
        sys-fs/sshfs-fuse   [[
            description = [ Browse the file system on your mobile device remotely ]
        ]]
    suggestion:
        dev-python/nautilus-python [[
            description = [ Add a context menu for sending file via KDE Connect ]
        ]]
"

if ever at_least 22.07.80 ; then
    DEPENDENCIES+="
        build:
            kde/plasma-wayland-protocols
        build+run:
            kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        suggestion:
            kde/kwin:4[wayland(+)] [[
                description = [ KWin's compositor is needed for the fake input interface ]
            ]]
    "
else
    DEPENDENCIES+="
        build+run:
            wayland? ( kde-frameworks/kwayland:5[>=${KF5_MIN_VER}] )
        suggestion:
            wayland? (
                kde/kwin:4[wayland(+)] [[
                    description = [ KWin's compositor is needed for the fake input interface ]
                ]]
            )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'wayland KF5Wayland' )
fi

DEFAULT_SRC_PREPARE_PATCHES+=( "${FILES}"/${PN}-tests-Use-QT_GUILESS_MAIN.patch )

CMAKE_SRC_CONFIGURE_PARAMS+=( -DNAUTILUS_PYTHON_EXTENSIONS_INSTALL_DIR=/usr/share/nautilus-python/extensions )
CMAKE_SRC_CONFIGURE_OPTIONS+=( 'bluetooth BLUETOOTH_ENABLED' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'doc KF5DocTools'
    'pulseaudio KF5PulseAudioQt'
)

kdeconnect_src_prepare() {
    kde_src_prepare

    # Tries to start kdeinit and other stuff and fails
    edo sed -e "/^ecm_add_test(sendfiletest.cpp/d" -i tests/CMakeLists.txt
}

kdeconnect_src_test() {
   if has_version --slash app-mobilephone/kdeconnect ; then
        ewarn "Skipping tests; they might break if you already have kdeconnect installed."
    else
        default
    fi
}

kdeconnect_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdeconnect_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

