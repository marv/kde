# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic
require github [ user=${PN} tag=v${PV} ]

SUMMARY="An SQLite extension that provides 256 bit AES encryption of database files"
HOMEPAGE="http://sqlcipher.net/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/tcl
        sys-libs/ncurses
        sys-libs/readline:=
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-fts3
    --enable-fts4
    --enable-fts5
    --enable-readline
    --enable-tcl
    --enable-tempstore=yes
    --disable-editline
    --disable-static-shell
)

src_prepare() {
    default

    # sad but true https://github.com/sqlcipher/sqlcipher#compiling
    # https://github.com/sqlcipher/sqlcipher/issues/391#issuecomment-847280410
    append-cppflags -DSQLITE_HAS_CODEC -DSQLCIPHER_TEST
}

src_test() {
    emake testfixture
    edo ./testfixture test/sqlcipher.test
}

