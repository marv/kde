# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the GNU General Public License

# Exlib for all packages released together as Plasma. See sets/plasma.conf
# for all packages which should use this exlib.

# Name of the package
myexparam pn=${MY_PN:-${PN}}
# Group or user in gitlab (invent.kde.org). Existing projects were imported in
# the 'kde' group with the gitlab migration, before moving them to their
# target group, resulting in a redirect. Ignored if no scm version
myexparam group_or_user=plasma
# Set to true if the package doesn't depend on x11-libs/qtbase:5
myexparam -b no_qt_dependency=false

branch="master"

if ever is_scm ; then
    if [[ ! -z $(ever range 3) ]] ; then
        branch="Plasma/$(ever range 1-2)"
    fi
fi

require kde.org [ pn=$(exparam pn) branch=${branch} group_or_user=$(exparam group_or_user) ]

if ! ever is_scm ; then
    case "$(ever range 3)" in
        90|95)
            DOWNLOADS="mirror://kde/unstable/plasma/$(ever range -3)/$(exparam pn)-${PV}.tar.xz"
            ;;
        *)
            DOWNLOADS="mirror://kde/stable/plasma/$(ever range -3)/$(exparam pn)-${PV}.tar.xz"
            ;;
    esac
fi

UPSTREAM_RELEASE_NOTES="https://kde.org/announcements/plasma/5/${PV%.0}.php"

if ! exparam -b  no_qt_dependency ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qtbase:5
    "
fi

